//
//  SWDetectionCameraVC.m
//  SWFaceDemo
//
//  Created by Gaurav Keshre on 4/3/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "SWDetectionCameraVC.h"
#import "SWPickUpViewController.h"
#import "SWConfirmDriverViewController.h"


@interface SWDetectionCameraVC (){
    BOOL cameraStateActive;
}
@end

@implementation SWDetectionCameraVC

- (void)createView {

}

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    NSLog(@"Stop user recognition process");
    [self.mobbIDView stopRecognition];
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    [self.mobbIDView removeFromSuperview];
    self.mobbIDView = nil;
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - BiometricMethodDelegate methods

- (void) enrollmentFinished:(MobbIDSDKOperationEnrollmentResult)resultCode
                       data:(MobbIDSDKOperationEnrollmentResultData *)data
                      error:(NSError *)errorOccurred {
    NSLog(@"Enrollment finished with resultCode = %d", resultCode);
    [self.delegate mobbIDViewEnrollmentFinishedWithResult:resultCode method:self.method name:self.title withUserID:data.userId withSelf:self error:errorOccurred];
    [self.navigationController popToViewController:(UIViewController *)self.delegate animated:YES];
}

- (void) verificationFinished:(MobbIDSDKOperationVerificationResult)resultCode
                         data:(MobbIDSDKOperationVerificationResultData *)data
                        error:(NSError *)errorOccurred {
    NSLog(@"Verification finished with resultCode = %d", resultCode);
    [self.delegate mobbIDViewVerificationFinishedWithResult:resultCode method:data.method token:data.token withUserID:data.userId error:errorOccurred];
    [self.navigationController popToViewController:(UIViewController *)self.delegate animated:YES];
}

- (void)verificationFinishedForUser:(NSString *)userUUID
                             result:(MobbIDSDKOperationVerificationResult)resultCode
                             method:(MobbIDSDKBiometricMethod)method
                              token:(NSString *)token
                              error:(NSError *)errorOccurred
{
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqual:@"nomatch"]) {
        SWPickUpViewController *pvc = segue.destinationViewController;
        [pvc setDriverData:sender];
    }else if([segue.identifier isEqual:@"match"]) {
        SWConfirmDriverViewController *cvc = segue.destinationViewController;
        [cvc setDriverDetail:sender];
    }
}

- (IBAction)handleLogout:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}


@end
