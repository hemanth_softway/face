//
//  SWDetectionCameraVC.h
//  SWFaceDemo
//
//  Created by Gaurav Keshre on 4/3/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobbIDFramework/MobbIDFramework.h>
@class SWDetectionCameraVC;
@protocol SWResultViewDelegate <NSObject>
@optional
- (void)mobbIDViewEnrollmentFinishedWithResult:(MobbIDSDKOperationEnrollmentResult)resultCode
                                        method:(MobbIDSDKBiometricMethod)method
                                          name:(NSString *)name
                                    withUserID:(NSString *)userID
                                      withSelf:(id)cameraVC
                                         error:(NSError *)errorOccurred;

- (void)mobbIDViewVerificationFinishedWithResult:(MobbIDSDKOperationVerificationResult)resultCode
                                          method:(MobbIDSDKBiometricMethod)method
                                           token:(NSString *)token
                                      withUserID:(NSString *)userID
                                           error:(NSError *)errorOccurred;
- (void)mobbIDViewIdentificationFinishedWithResult:(MobbIDSDKOperationIdentificationResult)resultCode
                                          method:(MobbIDSDKBiometricMethod)method
                                      withUserID:(NSString *)userID
                                          withSelf:(id)cameraVC
                                           error:(NSError *)errorOccurred;

@end

@interface SWDetectionCameraVC : UIViewController
<BiometricMethodDelegate>

@property (nonatomic, strong) id mobbIDView;

@property (nonatomic, weak) NSString *userId;
@property (nonatomic) MobbIDSDKRecognitionMode recognitionMode;
@property (nonatomic, weak) id <SWResultViewDelegate> delegate;
@property (nonatomic) MobbIDSDKBiometricMethod method;

- (void)createView;

@property (strong, nonatomic) NSDictionary *driverData;
@end
