//
//  SWMatchViewController.m
//  SWSampleDemo
//
//  Created by Hemanth on 02/04/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "SWMatchViewController.h"
#import "SWConfirmDriverViewController.h"

@interface SWMatchViewController ()

@end

@implementation SWMatchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"match"]) {
        SWConfirmDriverViewController *cvc = [segue destinationViewController];
        [cvc setDriverDetail:self.data];

    }}


@end
