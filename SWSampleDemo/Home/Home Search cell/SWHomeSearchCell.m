//
//  SWHomeSearchCell.m
//  SWSampleDemo
//
//  Created by Hemanth on 02/04/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "SWHomeSearchCell.h"

@implementation SWHomeSearchCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
