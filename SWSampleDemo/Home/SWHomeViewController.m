//
//  SWHomeViewController.m
//  SWSampleDemo
//
//  Created by Hemanth on 02/04/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "SWHomeViewController.h"
#import "SWHomeHeaderCell.h"
#import "SWHomePersonCell.h"
#import "SWHomeSearchCell.h"
#import "SWConfirmDriverViewController.h"
#import "SWVerificationCameraVC.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "AppUser.h"
#import "SW_CoreDataManager.h"

@interface SWHomeViewController ()<UITableViewDataSource, UITableViewDelegate,RetrieveUserInformationDelegate, SWResultViewDelegate>
{
 
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SWHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setBackBarButtonItem:nil];
    arrDrivers = [SW_CoreDataManager getObjectsForEntity:[AppUser entityName] withSortKey:@"name" andSortAscending:NO];
    
//    [DummyServices driversListForUser:@"dummy_user_01"
//                               school:@"dummy_school_01" complete:^(BOOL b, NSArray * result){
//                                   arrDrivers = [result mutableCopy];
//                                   [self.tableView reloadData];
//                               }];
    
  }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - TAbleView Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrDrivers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        SWHomePersonCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"SWHomePersonCell"];
    [cell setData:arrDrivers[indexPath.row]];
        return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SWHomeHeaderCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"SWHomeHeaderCell"];
    return cell;
}

#pragma mark - tv delegate Methods
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 93;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self performSegueWithIdentifier:@"selectDriverSegue" sender:arrDrivers[indexPath.row]];
}
- (IBAction)handlePickUP:(id)sender {
    //SWDetectionCameraVC
    SWVerificationCameraVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SWVerificationCameraVC"];
    vc.recognitionMode = MobbIDSDKRecognitionMode_Identification;
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)mobbIDViewVerificationFinishedWithResult:(MobbIDSDKOperationVerificationResult)resultCode
                                          method:(MobbIDSDKBiometricMethod)method
                                           token:(NSString *)token
                                      withUserID:(NSString *)userID
                                           error:(NSError *)errorOccurred
{
    
}
- (void)mobbIDViewIdentificationFinishedWithResult:(MobbIDSDKOperationIdentificationResult)resultCode method:(MobbIDSDKBiometricMethod)method withUserID:(NSString *)userID withSelf:(id)cameraVC error:(NSError *)errorOccurred
{
    
    if (resultCode == MobbIDSDKOperationIdentificationResult_USER_IDENTIFIED) {
        NSArray *results = [SW_CoreDataManager searchObjectsForEntity:[AppUser entityName] withPredicate:[NSPredicate predicateWithFormat:@"authID == %@ ", userID] andSortKey:nil andSortAscending:NO];
        AppUser *user = (AppUser *)results.lastObject;
        SWVerificationCameraVC *VC = (SWVerificationCameraVC *)cameraVC;
        [VC pushMatchVCWithDerivedData:user];
    }

}
#pragma mark - segue Methods

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"selectDriverSegue"]) {
        SWConfirmDriverViewController *cvc = (SWConfirmDriverViewController *)[segue destinationViewController];
        [cvc setDriverDetail:sender];        
    }
}
@end
