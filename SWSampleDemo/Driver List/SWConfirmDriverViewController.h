//
//  SWDriverListViewController.h
//  SWSampleDemo
//
//  Created by Hemanth on 02/04/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWBaseViewController.h"

@interface SWConfirmDriverViewController : SWBaseViewController
@property(nonatomic, strong)id driverDetail;
@property(nonatomic, copy)NSString *driverName;
@property(nonatomic, strong)UIImage *image;
@end
