//
//  SWDriverDetailCell.m
//  SWFaceDemo
//
//  Created by Gaurav Keshre on 4/3/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "SWDriverDetailCell.h"
#import "AppUser.h"

@interface SWDriverDetailCell()
@property (nonatomic, strong) AppUser *appUser;
@end
@implementation SWDriverDetailCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setData:(id)data{
    self.appUser = (AppUser *)data;
    [self.lblPersonName setText:[self.appUser.name componentsSeparatedByString:@" "][0]];
    [self.lblPersonDetail setText:[self.appUser.name componentsSeparatedByString:@" "][1]];
    [self.lblV setText:self.appUser.vehicle];
    NSString *str = [NSString stringWithFormat:@"%li",(long)[self.appUser.license_Plate integerValue]];

    [self.lblR setText:str];
    
}


@end
