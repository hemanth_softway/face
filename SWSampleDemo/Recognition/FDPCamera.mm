//
//  FDPCamera.m
//  GKFaceIdentifyDEmo
//
//  Created by Gaurav Keshre on 4/1/15.
//  Copyright (c) 2015 Softway Solutions Pvt. Ltd. All rights reserved.
//

#import "FDPCamera.h"

@implementation FDPCamera

#pragma mark -
#pragma mark Initialization and teardown

- (id)init;
{
    if (!(self = [super init]))
        return nil;
    
    // Grab the front-facing camera
    AVCaptureDevice * camera = nil;
//    NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil];

    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices) {
        if ([device position] == AVCaptureDevicePositionFront) {
            camera = device;
        }
    }
    
    // Create the capture session
    captureSession = [[AVCaptureSession alloc] init];
    
    // Add the video input
    NSError *error = nil;
    videoInput = [[AVCaptureDeviceInput alloc] initWithDevice:camera error:&error];
    if ([captureSession canAddInput:videoInput]) {
        [captureSession addInput:videoInput];
    }
    
    [self videoPreviewLayer];
    // Add the video frame output
    videoOutput = [[AVCaptureVideoDataOutput alloc] init];
    [videoOutput setAlwaysDiscardsLateVideoFrames:YES];
    // Use RGB frames instead of YUV to ease color processing
    
    [videoOutput setVideoSettings:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA] forKey:(id)kCVPixelBufferPixelFormatTypeKey]];
    
    //NOT SUPPORTED:
    //[videoOutput setVideoSettings:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_32RGBA] forKey:(id)kCVPixelBufferPixelFormatTypeKey]];
    
    //dispatch_queue_t videoQueue = dispatch_queue_create("com.sunsetlakesoftware.colortracking.videoqueue", NULL);
    //[videoOutput setSampleBufferDelegate:self queue:videoQueue];
    
    [videoOutput setSampleBufferDelegate:self queue:dispatch_get_main_queue()];
    
    if ([captureSession canAddOutput:videoOutput]) {
        [captureSession addOutput:videoOutput];
    } else {
#if defined(DEBUG)
        NSLog(@"Couldn't add video output");
#endif
    }
    
    // Start capturing
    //[captureSession setSessionPreset:AVCaptureSessionPresetHigh];
    [captureSession setSessionPreset:AVCaptureSessionPreset640x480];
    
    //DEBUG
    //[captureSession setSessionPreset:AVCaptureSessionPreset352x288];
    
    if (![captureSession isRunning]) {
        [captureSession startRunning];
    };
    
    return self;
}

- (void)dealloc
{
    [captureSession stopRunning];
    captureSession = nil;
    videoPreviewLayer = nil;
    videoOutput = nil;
    videoInput = nil;
}

#pragma mark -
#pragma mark AVCaptureVideoDataOutputSampleBufferDelegate

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    CVImageBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    [self.delegate processNewCameraFrame:pixelBuffer];
}

#pragma mark -
#pragma mark Accessors

@synthesize delegate;
@synthesize videoPreviewLayer;

- (AVCaptureVideoPreviewLayer *)videoPreviewLayer;
{
    if (videoPreviewLayer == nil) {
        videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:captureSession];
        [videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    }
    return videoPreviewLayer;
}


@end
