//
//  AppUser.m
//  SWFaceDemo
//
//  Created by Hemanth on 06/05/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "AppUser.h"


@implementation AppUser

@dynamic imagePath;
@dynamic authID;
@dynamic userID;
@dynamic name;
@dynamic index;
@dynamic guid;
@dynamic isActive;
@dynamic picture;
@dynamic age;
@dynamic eyeColor;
@dynamic gender;
@dynamic company;
@dynamic phone;
@dynamic address;
@dynamic about;
@dynamic registered;
@dynamic latitude;
@dynamic longitude;
@dynamic vehicle;
@dynamic license_Plate;

@end
