//
//  AppUser.h
//  SWFaceDemo
//
//  Created by Hemanth on 06/05/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "SW_BaseManagedObject.h"


@interface AppUser : SW_BaseManagedObject

@property (nonatomic, retain) NSString * imagePath;
@property (nonatomic, retain) NSString * authID;
@property (nonatomic, retain) NSString * userID;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * index;
@property (nonatomic, retain) NSString * guid;
@property (nonatomic, retain) NSNumber * isActive;
@property (nonatomic, retain) NSString * picture;
@property (nonatomic, retain) NSNumber * age;
@property (nonatomic, retain) NSString * eyeColor;
@property (nonatomic, retain) NSString * gender;
@property (nonatomic, retain) NSString * company;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * about;
@property (nonatomic, retain) NSString * registered;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSString * vehicle;
@property (nonatomic, retain) NSNumber * license_Plate;

@end
