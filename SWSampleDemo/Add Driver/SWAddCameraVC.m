//
//  SWAddCameraVC.m
//  SWFaceDemo
//
//  Created by Gaurav Keshre on 4/3/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "SWAddCameraVC.h"

@implementation SWAddCameraVC
- (void)createView {
    if (!self.mobbIDView){
        // we set the biometric method
        self.method = MobbIDSDKBiometricMethod_METHOD_FACE;
        // standard alloc/init with frame call for a UIView subclass
        self.mobbIDView = [[FaceView alloc] initWithFrame:self.cameraView.frame];
        // we set the delegate from our sdk view to this view controller
        [self.mobbIDView setDelegate:self];
        [self.mobbIDView setCaptureDelegate:self];
        // add the sdk view as a subview to the view controllers view
        [self.view addSubview:self.mobbIDView];
    }
}
- (void)faceSampleCaptured:(UIImage *)face
{
    self.capturImage = face;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.userInteractionEnabled = YES;
}
- (void)viewDidAppear:(BOOL)animated {
    [self createView];
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    NSLog(@"Start user recognition process");
    
    [self.mobbIDView startRecognitionIn:self.recognitionMode forUser:self.userId];
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)handleCapture:(id)sender {
   
    //    self.lblText.text = @"Please Wait...";
}

@end
