//
//  SWAddFaceHomeVV.m
//  SWFaceDemo
//
//  Created by Gaurav Keshre on 4/3/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "SWAddFaceHomeVV.h"
#import "SWAddCameraVC.h"
#import "AppDelegate.h"
#import "AppUser.h"
#import "SW_CoreDataManager.h"

@interface SWAddFaceHomeVV ()<CreateUserDelegate, ConfirmUserDelegate,SWResultViewDelegate>
{
    int selectedIndex;
    NSString *selectedUserId;
}
@end

@implementation SWAddFaceHomeVV

- (void)viewDidLoad {
    [super viewDidLoad];
    selectedIndex = -1;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableview Methods


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    selectedIndex = indexPath.row;
    AppUser *user = (AppUser *)arrDrivers[indexPath.row];
    selectedUserId=user.userID;
    [self.mobbIDManagementAPI createUserWithLanguage:MobbIDAPISupportedLanguage_English andGender:MobbIDAPIGender_Male delegate:self];
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    SWAddCameraVC *cvc = segue.destinationViewController;
    cvc.userId = self.userId;
    cvc.recognitionMode = MobbIDSDKRecognitionMode_Enrollment;
    cvc.delegate = self;

    [cvc setDriverData:sender];
}
#pragma mark - CreateUserDelegate
- (void)createUserFinished:(MobbIDAPICreateUserResult)result forUser:(NSString*)userId token:(NSString*)token error:(NSError *)errorOccurred
{
    [self hideProgress];
    if (result == MobbIDAPICreateUserResult_OK) {
        NSLog(@"User is created, but not yet confirmed. UserUUID: %@ , token: %@", userId, token);
        [self showProgress];
        [self.mobbIDManagementAPI confirmCreateUser:userId token:token delegate:self];
    }
    else {
        [self showError:errorOccurred];
    }
    
}
#pragma mark - ConfirmUserDelegate
- (void)confirmCreateUserFinished:(MobbIDAPIConfirmUserResult)result forUser:(NSString*)userId error:(NSError *)errorOccurred
{
    [self hideProgress];
    if (result == MobbIDAPIConfirmUserResult_OK) {
         self.userId = userId;
         [self performSegueWithIdentifier:@"addcamera" sender:arrDrivers[selectedIndex]];
    }
    else if (result == MobbIDAPIConfirmUserResult_ERROR && errorOccurred) {
        NSLog(@"Error while CONFIRMING user = %ld", (long)result);
        NSString *title = nil;
        NSString *message = nil;
        switch ([errorOccurred code]) {
            case ERROR_TOKEN_IS_NOT_VALID:
            case ERROR_USER_AND_TOKEN_COMBINATION_NOT_VALID:
                NSLog(@"The token is not the one created for the user. Error code: %ld", (long)[errorOccurred code]);
                title = NSLocalizedString(@"INVALID_TOKEN_TITLE", nil);
                message = NSLocalizedString(@"INVALID_TOKEN_TEXT", nil);
                break;
                
            case ERROR_TOKEN_HAS_EXPIRED:
                NSLog(@"The validity of the token has expired. Error code: %ld", (long)[errorOccurred code]);
                title = NSLocalizedString(@"EXPIRED_TOKEN_TITLE", nil);
                message = NSLocalizedString(@"EXPIRED_TOKEN_TEXT", nil);
                
            default:
                // Common errors
                NSLog(@"Generic error in the creation. Error code: %ld", (long)[errorOccurred code]);
                NSDictionary *dict = [self handleCommonMobbIDSDKErrors:errorOccurred];
                title = [dict objectForKey:@"title"];
                message = [dict objectForKey:@"message"];
                break;
        }
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
    
}
#pragma mark - SWResultViewDelegate
- (void)mobbIDViewEnrollmentFinishedWithResult:(MobbIDSDKOperationEnrollmentResult)resultCode method:(MobbIDSDKBiometricMethod)method name:(NSString *)name withUserID:(NSString *)userID withSelf:(id)cameraVC error:(NSError *)errorOccurred
{
    if (resultCode == MobbIDSDKOperationEnrollmentResult_USER_ENROLLED) {
        NSArray *results =  [SW_CoreDataManager searchObjectsForEntity:[AppUser entityName] withPredicate:[NSPredicate predicateWithFormat:@"userID == %@ ", selectedUserId] andSortKey:nil andSortAscending:NO];
        AppUser *user = (AppUser *)results.lastObject;
        user.authID = userID;
        SWAddCameraVC *vc = (SWAddCameraVC *) cameraVC;
        

        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
        NSDateFormatter *formatter;
        NSString        *dateString;
        
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
        
        dateString = [formatter stringFromDate:[NSDate date]];
        NSString *filePath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",dateString]]; //Add the file name
        NSData *pngData = UIImagePNGRepresentation(vc.capturImage);
        [pngData writeToFile:filePath atomically:YES]; //Write the file
        
        user.imagePath = filePath;
        NSError *error;
        if (![SW_CoreDataManager saveWithError:error]) {
            NSLog(@"Unable to save managed object context.");
            NSLog(@"%@, %@", error, error.localizedDescription);
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"User has successfully registered" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorOccurred.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];

    }
 
}
@end
