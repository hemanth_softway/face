//
//  StartIdentificationDelegate.h
//  MobbIDAPI
//
//  Created by Abraham Holgado on 15/04/13.
//
//

#import <Foundation/Foundation.h>

/**
 Possible return values for the [FaceAPI startIdentificationFinished:error] operation.
 */
typedef NS_ENUM(NSInteger, MobbIDAPIStartIdentificationResult) {
    /** The operation has been started correctly */
    MobbIDAPIStartIdentificationResult_OK,
    /** There has been some error with the operation.*/
    MobbIDAPIStartIdentificationResult_ERROR
};

/**
 Each identification process must begin with a startIdentificationdelegate: call to the FaceAPI.
 This delegate will be used to inform of the result of that call.
 
 @warning Identification mode is only currently available for the FaceAPI (both online and offline modes).
 */
@protocol StartIdentificationDelegate <NSObject>

/**
 Sent to the delegate when the identification has started.
 
 @param result The result of the operation.
 @param errorOccurred If an error has occurred, all the information will be available here. Possible error codes for this operation are (@see MobbIDAPIErrorCode):
 
 - MobbIDAPIErrorCode.ERROR_NOT_AVAILABLE_IN_CURRENT_MODE The operation is not available in the current operation mode specified in the configuration (MobbIDAPI) or for the specified biometric method.
 
 */
- (void) startIdentificationFinished:(MobbIDAPIStartIdentificationResult)result error:(NSError *)errorOccurred;

@end

