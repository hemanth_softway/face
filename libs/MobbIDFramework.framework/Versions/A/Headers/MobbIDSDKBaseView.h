//
//  MobbIDBaseView.h
//  MobbIDFramework
//
//  Created by Abraham Holgado García on 24/07/12.
//  Copyright (c) 2012 Mobbeel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BiometricMethodDelegate.h"
#import "MobbIDBaseAPI.h"
#import "MobbIDSDKTypes.h"

/**
 Base class for all biometric recognition views of the MobbID Framework
 
 @warning **All subclass must override startRecognition and stopRecognition methods.**
 */
@interface MobbIDSDKBaseView : UIView {
}

/**
 The delegate that will be informed of the result from the biometric recognition view.
 */
@property (nonatomic, weak) id <BiometricMethodDelegate> delegate;


/** The type of the verification that should be used by the view (simple, login or transaction). */
@property (nonatomic) MobbIDSDKVerificationType verificationType;

/**
 The operationId used to uniquely identify the operation. This must be specified if the verificationType specified is MobbIDSDKVerificationType_Transaction.
 
 It should be a UUID (xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxxx)
 */
@property (nonatomic, strong) NSString *operationId;

/**
 Indicates the type of verification attemp (unknown, genuine, casual_forgery or skilled_forgery).
 
 It is intented for internal used and will be only used when debug==YES.
 */
@property (nonatomic) MobbIDSDKVerificationSampleAttempt typeVerificationAttempt;

/** *For internal use*. It will hold the token returned by some API operations*/
@property (nonatomic, strong) NSString *token;

/** *For internal use*. It will hold the synchronizationToken returned by some API operations*/
@property (nonatomic, strong) NSString *synchronizationToken;

/** *For internal use*. It will hold the user's key returned by some API operations*/
@property (nonatomic, strong) NSString *key;

/** *For internal use*. It will hold the old token returned by some deprecated API operations*/
@property (nonatomic, strong) NSString *deprecatedToken;


/**
 This method kicks off the recognition process in the recognition mode (MobbIDSDKRecognitionMode) specified for the given userId.
 
 The result of the recognition operation will sent to the delegate object.
 
 @param mode Recognition mode (enrollment, verification or identification)
 @param userId Unique identifier for the user about to be enroll or verified. If the mode is MobbIDSDKRecognitionMode_Identification the value must be nil.
 @exception NSInternalInconsistencyException if this method in invoked other that in a subclass.
 
 @warning *Important*: This method must be overriden by the extending subclass.
 */
- (void)startRecognitionIn:(MobbIDSDKRecognitionMode)mode forUser:(NSString *)userId;

/**
 This method should be used if the recognition operation should be stopped.
 @exception NSInternalInconsistencyException if this method in invoked other that in a subclass.
 
 @warning *Important*: This method must be overriden by the extending subclass.
 */
- (void)stopRecognition;


@end
