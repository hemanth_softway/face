//
//  SignatureDataSet.h
//  BCSignatureC
//
//  Created by Abraham Holgado on 09/04/11.
//  Copyright 2011 Mobbeel Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 The SignatureDataSet class represents a signature as a sequential list of SignatureData points.
 */
@interface SignatureDataSet : NSObject<NSCoding> {
	
	NSMutableArray * rawDataPoints;
}

/**
 The array of traits that represents a signature itself. This array contains one array of SignatureData for each trait the signature has.
 */
@property (nonatomic, strong) NSMutableArray * rawDataPoints;

/**
 Adds a new trait to the object. 
    - A trait is the combination of sequential points that occurs since a pen-down and a pen-up event.
 @param trait The array of SignatureData points that belong to a same trait.
 */
- (void)addTrait:(NSArray*)trait;

@end
