//
//  FaceFaceVoiceView.h
//  MobbIDFramework
//
//  Created by Raul Jareño diaz on 16/07/12.
//  Copyright (c) 2012 Mobbeel. All rights reserved.
//
#import <UIKit/UIKit.h>

#import "MobbIDSDKBaseView.h"
#import "MobbIDSDKProgressUpdateDelegate.h"
#import "FaceView.h"

/**
 The delegate of a Face Voice capture sample View (FaceVoiceView) must adopt this protocol to receive the face and voice samples of the biometric view. 
 */
@protocol FaceVoiceCaptureSampleDelegate <NSObject>

@optional

/**
 This method gets called each time a face is capture. 
 
 If the resultCode is `MobbIDSDKOperationCaptureResult_DATA_CAPTURED` then the face will be avaiblable in the `face` parameter
 
 @param resultCode MobbIDSDKOperationCaptureResult The result of the capture operation.
 @param face UIImage Image of the user's face if the result of the operation `MobbIDSDKOperationCaptureResult_DATA_CAPTURED`
 @param errorOccurred NSError If an error has occurred when capturing the face all the information will be available here.

 */
- (void)faceSampleCaptureFinishedWithResult:(MobbIDSDKOperationCaptureResult)resultCode
                                   faceData:(UIImage*)face
                                       error:(NSError *)errorOccurred;
/**
 This method gets called each time a sample voice is capture. 
 
 If the resultCode is `MobbIDSDKOperationCaptureResult_DATA_CAPTURED` then the voice sample will be avaiblable in the `voice` parameter.

 @param resultCode MobbIDSDKOperationCaptureResult The result of the capture operation.
 @param voice NSData NSData containg the user's voice sample (encoded using .wav format) if the result of the operation is `MobbIDSDKOperationCaptureResult_DATA_CAPTURED`.
 @param errorOccurred NSError If an error has occurred when capturing the voice all the information will be available here. Possible error codes for this operation are (@see MobbIDSDKErrorCode):
 
 - MobbIDSDKErrorCode_ERROR_PERMISSION_DENIED

 */
- (void)voiceSampleCaptureFinishedWithResult:(MobbIDSDKOperationCaptureResult)resultCode
                                   voiceData:(NSData*)voice
                                       error:(NSError *)errorOccurred;
@end

/**
 FaceVoiceView is an special Biometric Recognition View that lets you verify an user using a combination of the face and the voice simultaneously to improve security or user convenience.
 
 Before performing the verification please ensure that:
 
 - The user must exist (@see MobbIDManagementAPI for more information) and must be enrolled in the Face *and* Voice methods. Use the FaceView and FaceVoiceView respectively to do so.
 - The SDK must has been configured properly (@see MobbIDAPI for more information).
 
 To use the FaceFaceVoiceView to verify an existing user you must follow these steps:
 
 1) Create and init FaceVoiceView instance ("self" would be the ViewController you're using to integrate the biometric view)
 
 `FaceVoiceView *faceVoiceView = [[FaceVoiceView alloc] initWithFrame:self.view.bounds];`
 
 2) Set up its delegate. It must conform the BiometricMethodDelegate protocol. The delegate does not have to be the ViewController (it could be any class conforming the BiometricMethodDelegate protocol)
 
 `[faceVoiceView setDelegate:self];'
 
 3) Add the FaceVoiceView as a subview to the ViewController you want to perform the biometric process.
 
 `[self.view addSubview:faceVoiceView];`
 
 4) Configure the verificationType you want. This property should be set to the same language that was used in the user creation @see MobbIDSDKVerificationType for more information.
 
 `[faceVoiceView setVerificationType:MobbIDSDKVerificationType_Simple];`
 
 5) Configure the language you want. This property should be set to the same language that was used in the user creation. @see MobbIDAPISupportedLanguage for more information.
 
 `[faceVoiceView setLanguage:language];`
 
 6) Kicks off the biometric recognition process using the method startRecognitionIn:forUser:
 
 `[faceVoiceView startRecognitionIn:MobbIDSDKRecognitionMode_Verification forUser:USERID];`
 
 7) Stop the biometric recognition process when it is finished (method verificationFinished:data:error: of the BiometricMethodDelegate will be called) using the method stopRecognition:
 
 `[faceVoiceView stopRecognition];`
 
 To use the FaceVoiceView to capture the samples you have to set up its `captureDelegate`, conforming to the `FaceVoiceCaptureSampleDelegate` protocol. The captureDelegate does not have to be the ViewController (it could be any class conforming the `FaceVoiceCaptureSampleDelegate`).
 
 `[faceVoiceView setCaptureDelegate:self];'
 

 If you don't want to perform the recognition you have to use the MobbIDSDKRecognitionMode_Capture in the method startRecognitionIn:forUser:
 
 `[faceVoiceView startRecognitionIn:MobbIDSDKRecognitionMode_Capture forUser:USERID];`
 
 @warning This biometric view does not let you enroll the users in Face or Voice methods.
 
 */
@interface FaceVoiceView : MobbIDSDKBaseView

/**
 This property should be set to the same language that was used in the user creation. If it is not, you could have an unexpected behaviour.
 
 @warning. This property would be obsolete in the next release
 */
@property (nonatomic, assign) MobbIDAPISupportedLanguage language;

/** 
 Specifies the speech mode that will be used in the enrollment (and later verifications) 
 */
@property (nonatomic, assign) MobbIDSDKVoiceRecognitionSpeechMode speechMode;

/** 
 YES if flac encoding should be used 
 */
@property (nonatomic, assign) BOOL useFlacEncoding;

/** 
 Set this deleate if you want to capture the samples for voice and face using this view. Remember you have to use the `MobbIDSDKRecognitionMode_Capture` in the `startRecognitionIn:forUser:` method
 */
@property (nonatomic, weak) id<FaceVoiceCaptureSampleDelegate> captureDelegate;

/**
 Set this delegate if you want to override the progress update default behavior (with progress bar) and be informed of the face recognition progress.
 */
@property (nonatomic, weak) id<MobbIDSDKProgressUpdateDelegate> faceProgressUpdateDelegate;

/**
 Set this delegate if you want to override the progress update default behavior (with progress bar) and be informed of the voice recognition progress.
 */
@property (nonatomic, weak) id<MobbIDSDKProgressUpdateDelegate> voiceProgressUpdateDelegate;

/**
 Set this delegate if you want to be informed of the face detection process.
 */
@property (nonatomic, weak) id<FaceDetectionDelegate> faceDetectionDelegate;


@end
