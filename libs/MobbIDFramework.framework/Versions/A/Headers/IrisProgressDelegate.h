//
//  IrisProgressDelegate.h
//  MobbIDAPI
//
//  Created by Abraham Holgado on 16/04/13.
//
//

#import <Foundation/Foundation.h>

#import "IrisAPI.h"

/**
 To be documented!
 */
@protocol IrisProgressDelegate <NSObject>

/**
 Sent to the delegate when the system has isolated the two eyes from the image.
 
 @param eyes An array of one or two UIImage representing the eye(s) isolated.
 @param eyeRects An array of one or two CGRect representing the eye(s) image position.
 
 */
- (void) eyesDetected:(NSArray*)eyes withRects:(NSArray *)eyeRects;

/**
 Sent to the delegate when an iris has been detected and segmented.
 
 @param irisRect The rectangle representing the limbus boundary.
 @param pupilRect The rectangle representing the pupil bondary.
 @param info Indicates if the eye is left, right or unknown.
 
 */
- (void) irisDetectedWith:(CGRect)irisRect andPupil:(CGRect)pupilRect eye:(EyeInformation)info;

@end
