//
//  StartVerificationDelegate.h
//  MobbIDAPI
//
//  Created by Abraham Holgado on 12/04/13.
//
//

#import <Foundation/Foundation.h>

/**
 Possible return values for the biometricAPI (FaceAPI, VoiceAPI, SignatureAPI...) startVerification:delegate: operation.
 */
typedef NS_ENUM(NSInteger, MobbIDAPIStartVerificationResult) {
    /** The operation has been started properly */
    MobbIDAPIStartVerificationResult_OK,
    /** There has been some error with the operation */
    MobbIDAPIStartVerificationResult_ERROR
};

/**
 Each verification process must begin with a startVerification:delegate: call to their corresponding API. 
 This delegate will be used to inform of the result of this call.
 */
@protocol StartVerificationDelegate <NSObject>

/**
 Sent to the delegate when the verification process has started.
 
 @param result The result of the operation.
 @param userId The userId of the user that is about to be verified.
 @param errorOccurred If an error has occurred, all the information will be available here. Possible error codes for this operation are (@see MobbIDAPIErrorCode):
 
 - MobbIDAPIErrorCode.ERROR_USER_FIELD_REQUIRED The userId is mandatory.
 - MobbIDAPIErrorCode.ERROR_USER_DOES_NOT_EXIST The userId specified does not belong to any registered user.
 - MobbIDAPIErrorCode.ERROR_NOT_AVAILABLE_IN_CURRENT_MODE The operation is not available in the current operation mode specified in the configuration (MobbIDAPI) or for the specified biometric method.
 - MobbIDAPIErrorCode.ERROR_METHOD_NOT_AVAILABLE_FOR_THIS_LICENSE
 - MobbIDAPIErrorCode.ERROR_USER_IS_NOT_ACTIVATED
 
 */
- (void) startVerificationFinished:(MobbIDAPIStartVerificationResult)result user:(NSString *)userId error:(NSError *)errorOccurred;

@end
