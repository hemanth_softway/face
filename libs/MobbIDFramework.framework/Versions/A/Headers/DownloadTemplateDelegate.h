//
//  DownloadTemplateDelegate.h
//  MobbIDAPI
//
//  Created by Raul Jareño diaz on 01/07/13.
//
//

#import <Foundation/Foundation.h>

/**
 Possible return values for the [DownloadTemplateDelegate downloadTemplateFinished:userId:bioTemplate:biometricMethod:timestamp:enrollType:error] operation.
 */
typedef NS_ENUM(NSInteger, MobbIDSDKDownloadTemplateResult)  {
    /** Template synchronized */
    MobbIDSDKDownloadTemplateResult_OK,
    /** There has been some error with the operation */
    MobbIDSDKDownloadTemplateResult_ERROR
};

/**
 Delegate to inform of the result of the downloadTemplate method
 */
@protocol DownloadTemplateDelegate <NSObject>

/**
 Sent to the delegate when the download operation has finished.
 
 TODO. Document parameters!
 
 @param result The result of the download process
 @param userId The user of the biometric template
 @param biometricTemplate The biometric template itself. Its format depends on the biometricMethod
 @param biometricMethod The biometric method of the template. It could take one the these values:
 
 - kMobbIDAPI_BiometricMethod_Signature
 - kMobbIDAPI_BiometricMethod_Voice
 - kMobbIDAPI_BiometricMethod_Face
 
 @param timestamp Epoch time (amount in milliseconds since 1 of January of 1970)
 @param enrollType Type of enrollment (it is only used for voice method).
 @param errorOccurred If an error has occurred, all the information will be available here. Possible error codes for this operation are (@see MobbIDAPIErrorCode):
 
 - MobbIDAPIErrorCode.ERROR_UNEXPECTED
 
 */
- (void) downloadTemplateFinished:(MobbIDSDKDownloadTemplateResult)result
                           userId:(NSString *)userId
                      bioTemplate:(NSData *)biometricTemplate
                  biometricMethod:(NSString *)biometricMethod
                        timestamp:(NSString *)timestamp
                       enrollType:(NSString *)enrollType
                            error:(NSError *)errorOccurred;

@end